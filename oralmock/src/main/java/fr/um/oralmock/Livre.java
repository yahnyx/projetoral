package fr.um.oralmock;

import java.util.ArrayList;

public class Livre {
	private String isbn;
	private String titre;
	private int siecle;
	private Categorie categorie;
	
	private ArrayList<Lecteur> lecteurs;
	private ArrayList<Lecteur> lecteursSatisfaits;
	private ArrayList<Lecteur> lecteursNonSat;
	
	// un attribut statique pour stocker tous les livres que l'on instancie
	public static ArrayList<Livre> TousLesLivres = new ArrayList<Livre>();
	//méthode qui reinitialise notre liste de livre instancié (qui va nous servir dans les tests)
	public static void resetLivres() {TousLesLivres= new ArrayList<Livre>();}

	//Constructeurs
	
	public Livre(String isbn, String titre, int siecle, Categorie categorie) {
		super();
		this.isbn = isbn;
		this.titre = titre;
		this.siecle = siecle;
		this.categorie = categorie;
		this.lecteurs = new ArrayList<Lecteur>();
		this.lecteursSatisfaits = new ArrayList<Lecteur>();
		this.lecteursNonSat = new ArrayList<Lecteur>();
		
		TousLesLivres.add(this);
	}

	public Livre(String isbn, String titre, int siecle, String categorie) {
		this(isbn,titre,siecle,Categorie.stringToCategorie(categorie));
	}
	
	//Getters and setters
	
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public int getSiecle() {
		return siecle;
	}
	
	public String setSiecle(int siecle) {
		if (siecleValide(siecle)) {
			this.siecle = siecle;
			return "siècle ajouté";
		}else {
			return "le siècle n'est pas valide";
		}
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public ArrayList<Lecteur> getLecteurs() {
		return lecteurs;
	}

	public void setLecteurs(ArrayList<Lecteur> lecteurs) {
		this.lecteurs = lecteurs;
	}

	public ArrayList<Lecteur> getLecteursSatisfaits() {
		return lecteursSatisfaits;
	}

	public void setLecteursSatisfaits(ArrayList<Lecteur> lecteursSatisfaits) {
		this.lecteursSatisfaits = lecteursSatisfaits;
	}
	
	public ArrayList<Lecteur> getLecteursNonSat() {
		return lecteursNonSat;
	}

	public void setLecteursNonSat(ArrayList<Lecteur> lecteursNonSat) {
		this.lecteursNonSat = lecteursNonSat;
	}
	
	public void addLecteur(Lecteur lecteur) {
		lecteurs.add(lecteur);
	}
	
	public void addLecteursSatisfait(Lecteur lecteur) {
		lecteursSatisfaits.add(lecteur);
	}
	
	public void addLecteursNonSat(Lecteur lecteur) {
		lecteursNonSat.add(lecteur);
	}
	
	//méthode non-implémentée que l'on va utiliser avec nos mocks
	// la méthode retire simple un lecteur de la liste des lecteurs satisfaits du livre
	public void removeLecteursSatisfait(Lecteur lecteur) {
		throw new UnsupportedOperationException();
	}
	
	// méthode pour vérifier qu'un siècle est valide
	
	public static boolean siecleValide(int siecle) {
		return siecle<21;
	}
	
	
	public static ArrayList<Livre> recommandations(Lecteur l)
	{
		ArrayList<Livre> res = new ArrayList<Livre>();
		
		ArrayList<Categorie> cates = l.getCategorieAimees();
		
		for(Livre li:TousLesLivres)
		{
			if(cates.contains(li.categorie) )//&& !(l.getLivresLus().contains(li)))
			{
				res.add(li);
			}
		}
		
		return res;
	}
}
